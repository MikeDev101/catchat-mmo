# CatChat! Meow around and make new pals!
Project CatChat - MMO Game inspired by Club Penguin

This is the official github repository for the CatChat MMO.

CatChat is a cloud-based MMO game, built upon The C.A.T (Short for Cloud Assisted mulTiplayer) Engine. Inspired by Club Penguin, it's an IRC (Internet Relay Chat) Style type of game. CatChat was originally made using Scratch 3.0.

Now, the upcomming Godot Edition will be using, well, Godot.

Scratch is a project of the Scratch Foundation, in collaboration with the Lifelong Kindergarten Group at the MIT Media Lab. It is available for free at https://scratch.mit.edu

Our original Scratch forums can be found using https://scratch.mit.edu/discuss/topic/345604
