extends Area2D

export var speed = 360  # How fast the player will move (pixels/sec).
var screen_size  # Size of the game window.
var dir = 0
var allow_input = true

func _ready():
    screen_size = get_viewport_rect().size

func _process(delta):
	if allow_input == true:
		var velocity = Vector2()  # The player's movement vector.
		if Input.is_action_pressed("ui_right"):
			dir = 0
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			dir = 1
			velocity.x -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed
			$AnimatedSprite.animation = "run"
		else:
			$AnimatedSprite.animation = "idle"
		if dir == 1:
			$AnimatedSprite.flip_h = true
		else:
			$AnimatedSprite.flip_h = false
		position += velocity * delta
		position.x = clamp(position.x, 0, screen_size.x)
		position.y = clamp(position.y, 0, screen_size.y)

func change_allow_input():
	if allow_input == false:
		allow_input = true
	else:
		allow_input = true