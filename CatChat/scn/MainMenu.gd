extends Node2D

var meow = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ConnectButton_pressed():
	meow = true
	$Node/Meow.play()
	pass # Replace with function body.

func _on_Soundtrack_finished():
	$Node/Soundtrack.play()
	pass # Replace with function body.

func DoneMeowing():
	if meow == true:
		get_tree().change_scene("res://scn/World.tscn")
	pass # Replace with function body.
